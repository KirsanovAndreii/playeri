package com.ankir.playiawaketech.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ankir.playiawaketech.R;
import com.ankir.playiawaketech.api.RetrofitGetProgram;
import com.ankir.playiawaketech.model.ModelForAdapterPrograms;
import com.ankir.playiawaketech.model.ModelForAdapterTracks;
import com.ankir.playiawaketech.model.ModelFree;
import com.ankir.playiawaketech.utils.AdapterListPrograms;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private String TAG = "TAG debug  ";
    private ModelFree responseFree;
    private List<ModelForAdapterPrograms> listPrograms = new ArrayList<>();
    private List<ModelForAdapterTracks> listTracks = new ArrayList<>();
    private AdapterListPrograms adapterList;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.main_list_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getListProgramsFromServer();

        adapterList = new AdapterListPrograms(this, listPrograms);
        listView.setAdapter(adapterList);
        //  adapterList.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                listTracks.clear();
                for (int j = 0; j < responseFree.getPrograms().get(position).getTracks().size(); j++) {
                    ModelForAdapterTracks mfa = new ModelForAdapterTracks();
                    mfa.setTitleTrack(responseFree.getPrograms().get(position).getTracks().get(j).getTitle());
                    mfa.setUrlTrack(responseFree.getPrograms().get(position).getTracks().get(j).getMedia().getMp3().getUrl());
                    listTracks.add(mfa);
                }

                Intent intent = new Intent(MainActivity.this, ListTracksActivity.class);
                intent.putExtra("KEY", (ArrayList) listTracks);
                startActivity(intent);
            }
        });
    }

    private void getListProgramsFromServer() {

        RetrofitGetProgram retrofitGetProgram = RetrofitGetProgram.getInstance();
        retrofitGetProgram.sendLogout().enqueue(new Callback<ModelFree>() {
            @Override
            public void onResponse(Call<ModelFree> call, Response<ModelFree> response) {
                if (response.isSuccessful()) {
                    responseFree = response.body();

                    listPrograms.clear();
                    for (int i = 0; i < responseFree.getPrograms().size(); i++) {
                        ModelForAdapterPrograms mfa = new ModelForAdapterPrograms();
                        mfa.setTitleProgram(responseFree.getPrograms().get(i).getTitle());
                        mfa.setIconProgram(responseFree.getPrograms().get(i).getCover().getResolutions().get(1).getUrl());
                        listPrograms.add(mfa);

                        Log.d(TAG + i + " Program title ", responseFree.getPrograms().get(i).getTitle() + "");
                        Log.d(TAG + i + " Program cover ", responseFree.getPrograms().get(i).getCover().getResolutions().get(1).getUrl() + "");
                        for (int j = 0; j < responseFree.getPrograms().get(i).getTracks().size(); j++) {
                            Log.d(TAG + j + " track title ", responseFree.getPrograms().get(i).getTracks().get(j).getTitle() + "");
                            Log.d(TAG + j + " track mp3 ", responseFree.getPrograms().get(i).getTracks().get(j).getMedia().getMp3().getUrl() + "");
                        }
                        adapterList.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelFree> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Что-то пошло не так. Попробуйте еще раз", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
