package com.ankir.playiawaketech.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.ankir.playiawaketech.R;
import com.ankir.playiawaketech.model.ModelForAdapterTracks;
import com.ankir.playiawaketech.utils.AdapterListTracks;

import java.util.ArrayList;
import java.util.List;

public class ListTracksActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {

    private List<ModelForAdapterTracks> listTracksFromIntent = new ArrayList<>();
    private AdapterListTracks adapterList;
    private ListView listView;
    private MediaPlayer mediaPlayer = null;
    private ImageButton playPauseButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_list_tracks);
        listView = findViewById(R.id.second_list_tracks);
        playPauseButton = findViewById(R.id.image_button_play_pause);

        playPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    Toast.makeText(ListTracksActivity.this, "Pause", Toast.LENGTH_SHORT).show();
                } else {
                    mediaPlayer.start();
                    Toast.makeText(ListTracksActivity.this, "Play", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        playPauseButton.setVisibility(View.INVISIBLE);
        listTracksFromIntent = (ArrayList<ModelForAdapterTracks>) getIntent().getSerializableExtra("KEY");

        adapterList = new AdapterListTracks(this, listTracksFromIntent);
        listView.setAdapter(adapterList);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                }

                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                try {
                    mediaPlayer.setDataSource(listTracksFromIntent.get(position).getUrlTrack());
                    mediaPlayer.setOnPreparedListener(ListTracksActivity.this);
                    mediaPlayer.prepareAsync(); // might take long! (for buffering, etc)

                } catch (Exception e) {
                    Toast.makeText(ListTracksActivity.this, "Composition is missing", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                playPauseButton.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
        mediaPlayer.release();
    }
}
