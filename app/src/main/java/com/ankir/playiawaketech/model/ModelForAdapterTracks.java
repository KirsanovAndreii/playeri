package com.ankir.playiawaketech.model;

import java.io.Serializable;

public class ModelForAdapterTracks implements Serializable {
    private String titleTrack;
    private String urlTrack;

    public String getTitleTrack() {
        return titleTrack;
    }

    public void setTitleTrack(String titleTrack) {
        this.titleTrack = titleTrack;
    }

    public String getUrlTrack() {
        return urlTrack;
    }

    public void setUrlTrack(String urlTrack) {
        this.urlTrack = urlTrack;
    }
}
