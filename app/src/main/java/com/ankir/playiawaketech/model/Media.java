
package com.ankir.playiawaketech.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_type",
    "mp3",
    "flac",
    "m4a"
})
public class Media {

    @JsonProperty("_type")
    private String type;
    @JsonProperty("mp3")
    private Mp3 mp3;
    @JsonProperty("flac")
    private Flac flac;
    @JsonProperty("m4a")
    private M4a m4a;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("_type")
    public String getType() {
        return type;
    }

    @JsonProperty("_type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("mp3")
    public Mp3 getMp3() {
        return mp3;
    }

    @JsonProperty("mp3")
    public void setMp3(Mp3 mp3) {
        this.mp3 = mp3;
    }

    @JsonProperty("flac")
    public Flac getFlac() {
        return flac;
    }

    @JsonProperty("flac")
    public void setFlac(Flac flac) {
        this.flac = flac;
    }

    @JsonProperty("m4a")
    public M4a getM4a() {
        return m4a;
    }

    @JsonProperty("m4a")
    public void setM4a(M4a m4a) {
        this.m4a = m4a;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
