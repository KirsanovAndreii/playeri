
package com.ankir.playiawaketech.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_type",
    "id",
    "title",
    "isFree",
    "isAvailable",
    "isFeatured",
    "banner",
    "cover",
    "headphones",
    "descriptionHTML",
    "tracks"
})
public class Program {

    @JsonProperty("_type")
    private String type;
    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("isFree")
    private Boolean isFree;
    @JsonProperty("isAvailable")
    private Boolean isAvailable;
    @JsonProperty("isFeatured")
    private Boolean isFeatured;
    @JsonProperty("banner")
    private Banner banner;
    @JsonProperty("cover")
    private Cover cover;
    @JsonProperty("headphones")
    private Boolean headphones;
    @JsonProperty("descriptionHTML")
    private String descriptionHTML;
    @JsonProperty("tracks")
    private List<Track> tracks = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("_type")
    public String getType() {
        return type;
    }

    @JsonProperty("_type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("isFree")
    public Boolean getIsFree() {
        return isFree;
    }

    @JsonProperty("isFree")
    public void setIsFree(Boolean isFree) {
        this.isFree = isFree;
    }

    @JsonProperty("isAvailable")
    public Boolean getIsAvailable() {
        return isAvailable;
    }

    @JsonProperty("isAvailable")
    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    @JsonProperty("isFeatured")
    public Boolean getIsFeatured() {
        return isFeatured;
    }

    @JsonProperty("isFeatured")
    public void setIsFeatured(Boolean isFeatured) {
        this.isFeatured = isFeatured;
    }

    @JsonProperty("banner")
    public Banner getBanner() {
        return banner;
    }

    @JsonProperty("banner")
    public void setBanner(Banner banner) {
        this.banner = banner;
    }

    @JsonProperty("cover")
    public Cover getCover() {
        return cover;
    }

    @JsonProperty("cover")
    public void setCover(Cover cover) {
        this.cover = cover;
    }

    @JsonProperty("headphones")
    public Boolean getHeadphones() {
        return headphones;
    }

    @JsonProperty("headphones")
    public void setHeadphones(Boolean headphones) {
        this.headphones = headphones;
    }

    @JsonProperty("descriptionHTML")
    public String getDescriptionHTML() {
        return descriptionHTML;
    }

    @JsonProperty("descriptionHTML")
    public void setDescriptionHTML(String descriptionHTML) {
        this.descriptionHTML = descriptionHTML;
    }

    @JsonProperty("tracks")
    public List<Track> getTracks() {
        return tracks;
    }

    @JsonProperty("tracks")
    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
