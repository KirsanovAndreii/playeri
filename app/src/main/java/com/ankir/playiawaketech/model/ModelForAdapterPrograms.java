package com.ankir.playiawaketech.model;

import java.io.Serializable;


public class ModelForAdapterPrograms  implements Serializable {
    private String titleProgram;
    private String iconProgram;


    public ModelForAdapterPrograms() {
    }

    public String getTitleProgram() {
        return titleProgram;
    }

    public void setTitleProgram(String titleProgram) {
        this.titleProgram = titleProgram;
    }

    public String getIconProgram() {
        return iconProgram;
    }

    public void setIconProgram(String iconProgram) {
        this.iconProgram = iconProgram;
    }
}





