package com.ankir.playiawaketech.api;

import com.ankir.playiawaketech.model.ModelFree;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;


public class RetrofitGetProgram {
    private static final String ENDPOINT = "https://api.iawaketechnologies.com";
    private static ApiInterface apiInterface;

    private static RetrofitGetProgram instance = new RetrofitGetProgram();

    private RetrofitGetProgram() {
    }

    public static RetrofitGetProgram getInstance() {
        return instance;
    }

    {
        initialize();
    }

    interface ApiInterface {
        @GET("/api/v2/media-library/free")
        Call<ModelFree> getResponse();
    }

    public  void initialize() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    public  Call<ModelFree> sendLogout() {
        return apiInterface.getResponse();
    }
}
