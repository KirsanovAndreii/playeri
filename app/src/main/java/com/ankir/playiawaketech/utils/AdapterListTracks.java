package com.ankir.playiawaketech.utils;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.ankir.playiawaketech.R;


import com.ankir.playiawaketech.model.ModelForAdapterTracks;


import java.util.List;




public class AdapterListTracks extends ArrayAdapter<ModelForAdapterTracks> {
    Context context;

    public AdapterListTracks(@NonNull Context context, @LayoutRes List<ModelForAdapterTracks> values) {
        super(context, R.layout.item_tracks, values);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.item_tracks, parent, false);
            holder = new ViewHolder();

            holder.track_title = (TextView) rowView.findViewById(R.id.track_title);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.track_title.setText(getItem(position).getTitleTrack());
        return rowView;
    }

    class ViewHolder {
        public TextView track_title;
    }
}


