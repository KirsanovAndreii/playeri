package com.ankir.playiawaketech.utils;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ankir.playiawaketech.R;
import com.ankir.playiawaketech.model.ModelForAdapterPrograms;

import com.ankir.playiawaketech.ui.MainActivity;
import com.squareup.picasso.Picasso;

import java.util.List;




public class AdapterListPrograms extends ArrayAdapter<ModelForAdapterPrograms> {
    Context context;

    public AdapterListPrograms(@NonNull Context context, @LayoutRes List<ModelForAdapterPrograms> values) {
        super(context, R.layout.item_program, values);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.item_program, parent, false);
            holder = new ViewHolder();
            holder.program_image = (ImageView) rowView.findViewById(R.id.program_image);
            holder.program_title = (TextView) rowView.findViewById(R.id.program_title);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        Picasso.with(context)
                .load(getItem(position).getIconProgram())
                .placeholder(R.drawable.i)
                .error(R.drawable.i)
                .into(holder.program_image);

        holder.program_title.setText(getItem(position).getTitleProgram());

        return rowView;
    }

    class ViewHolder {
        public ImageView program_image;
        public TextView program_title;
    }
}

